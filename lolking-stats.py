import urllib2
from bs4 import BeautifulSoup
import sys
import re
import operator

profileSoup = BeautifulSoup(urllib2.urlopen('http://www.lolking.net/summoner/na/23536969').read())

foundHistory = False

for junk in profileSoup.find_all('div', {'class':'pane_inner'}):
   if (junk.find('div', {'class':'match_loss'}) != None or junk.find('div', {'class':'match_win'}) != None):
      foundHistory = True
      break;

if not foundHistory:
   sys.exit(1)
   
for game in junk.find_all('div',{'class':'match_loss'}) + junk.find_all('div',{'class':'match_win'}):
   message = ''
   mode = game.find('div',{'style':'font-size: 12px; font-weight: bold;'}).text
   champ = re.search('\/\w+\/(\w+)', game.find('a',href=True)['href']).group(1).capitalize()
   if (game.find(text='Loss') != None):
      result = "Defeat"
   elif (game.find(text='Win') != None):
      result = "Victory"
   else:
      result = "Tie"
   kills,deaths,assists = operator.itemgetter(0,3,6)(game.find('div',{'style':'width: 70px;'}).find_all())
   #for stats in game.find('div',{'style':'width: 70px;'}).find_all():
   #   if  len(stats.text) != 0:
   #      message += stats.text + " "
   message += mode + " - " + result + " - " + champ + ": K/D/A " + "/".join([kills.text,deaths.text,assists.text])
   print message